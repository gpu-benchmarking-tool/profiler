/*__________________________________________________
 |                                                  |
 |  File: ProfilerTests.cpp                          |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Profiler Unit tests.               |
 |_________________________________________________*/



#include "gtest/gtest.h"
#include "../../src/Profiler.h"


TEST(Profiler, AddEntry) {
    gpu::Profiler p;
    p.addTrianglesEntry(50, 207);
}
