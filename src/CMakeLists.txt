cmake_minimum_required(VERSION 2.8)
project(profiler-library)

set(SOURCE_FILES Profiler.cpp)

add_library(profiler-library ${SOURCE_FILES})
set_target_properties(profiler-library PROPERTIES SUFFIX ".a")
install(TARGETS profiler-library DESTINATION ${INSTALL_BIN_DIR})
