/*__________________________________________________
 |                                                  |
 |  File: Profiler.cpp                              |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Profiler used to get the GPU data  |
 |  during the tests.                               |
 |_________________________________________________*/



#pragma once

#include <vector>
#include <string>
#include <fstream>
#include "GLInfo.hpp"
#include "VkInfo.hpp"


namespace gpu {


/**
 * @brief      Class for profiler.
 * Saves the GPU information: vendor, renderer.
 * OpenGL version, shading version, extensions.
 * Vulkan device name, API version, driver version, vendor ID, device ID, extensions.
 * Std::vector stores Profiling data (polycount per time).
 */
class Profiler {

  private:
    std::vector<std::vector<unsigned>> data;
    std::string vendor, renderer, glVersion, glShadingVersion, glExtensions, vkDeviceName;
    uint32_t vkApiVersion, vkDriverVersion, vkVendorID, vkDeviceID;
    std::vector<std::string> vkExtensions;

  public:

    GLInfo glInfo;
    VkInfo vkInfo;
    enum TYPE { TRIANGLES };

    /**
     * @brief      Constructs the object.
     * Empty constructor.
     */
    Profiler() {}


    /**
     * @brief      Destroys the object.
     */
    ~Profiler() {}


    /**
     * @brief      Save data to csv.
     * Saves the data gathered into a CSV file.
     *
     * @param[in]  filename  The filename
     *
     * @return     True if save is successful, False otherwise.
     */
    bool save(std::string filename);


    /**
     * @brief      Clear profiler.
     * Deletes the data vector.
     */
    void clear() { data.clear(); }


    /**
     * @brief      Sets the GPU vendor.
     *
     * @param[in]  data  The data
     */
    void setVendor(std::string data) { vendor = data; }


    /**
     * @brief      Sets the renderer.
     *
     * @param[in]  data  The data
     */
    void setRenderer(std::string data) { renderer = data; }


    /**
     * @brief      Sets the gl version.
     *
     * @param[in]  data  The data
     */
    void setGLVersion(std::string data) { glVersion = data; }


    /**
     * @brief      Sets the gl shading language version.
     *
     * @param[in]  data  The data
     */
    void setGLShadingLanguageVersion(std::string data) { glShadingVersion = data; }


    /**
     * @brief      Sets the gl extensions.
     *
     * @param[in]  data  The data
     */
    void setGLExtensions(std::string data) { glExtensions = data; }


    /**
     * @brief      Sets the vulkan device name.
     *
     * @param[in]  data  The data
     */
    void setVkDeviceName(std::string data) { vkDeviceName = data; }


    /**
     * @brief      Sets the vulkan api version.
     *
     * @param[in]  data  The data
     */
    void setVkApiVersion(uint32_t data) { vkApiVersion = data; }


    /**
     * @brief      Sets the vulkan driver version.
     *
     * @param[in]  data  The data
     */
    void setVkDriverVersion(uint32_t data) { vkDriverVersion = data; }


    /**
     * @brief      Sets the vulkan vendor id.
     *
     * @param[in]  data  The data
     */
    void setVkVendorID(uint32_t data) { vkVendorID = data; }


    /**
     * @brief      Sets the vulkan device id.
     *
     * @param[in]  data  The data
     */
    void setVkDeviceID(uint32_t data) { vkDeviceID = data; }


    /**
     * @brief      Sets the vulkan extensions.
     *
     * @param[in]  extensions  The extensions
     */
    void setVkExtensions(std::vector<std::string> extensions) { vkExtensions = extensions; }


    /**
    * @brief      Adds an entry to the profiler.
     * Add en entry given a polycount and the time taken to render the frame.
    *
    * @param[in]  numberOfTriangles  The number of triangles
    * @param[in]  time               The time taken to render a frame
    */
    void addTrianglesEntry(unsigned numberOfTriangles, unsigned time);


    /**
     * @brief      Gets the data recorded.
     *
     * @return     The data recorded until now.
     */
    std::vector<std::vector<unsigned>> getData() { return data; }


    /**
     * @brief      Gets the GPU vendor.
     *
     * @return     The data.
     */
    std::string getVendor() { return vendor; }


    /**
     * @brief      Gets the renderer.
     *
     * @return     The data.
     */
    std::string getRenderer() { return renderer; }


    /**
     * @brief      Gets the gl version.
     *
     * @return     The data.
     */
    std::string getGLVersion() { return glVersion; }


    /**
     * @brief      Gets the gl shading language version.
     *
     * @return     The data.
     */
    std::string getGLShadingLanguageVersion() { return glShadingVersion; }


    /**
     * @brief      Gets the gl extensions.
     *
     * @return     The data.
     */
    std::string getGLExtensions() { return glExtensions; }


    /**
     * @brief      Gets the vulkan device name.
     *
     * @return     The vulkan device name.
     */
    std::string getVkDeviceName() { return vkDeviceName; }


    /**
     * @brief      Gets the vulkan api version.
     *
     * @return     The vulkan api version.
     */
    uint32_t getVkApiVersion() { return vkApiVersion; }


    /**
     * @brief      Gets the vulkan driver version.
     *
     * @return     The vulkan driver version.
     */
    uint32_t getVkDriverVersion() { return vkDriverVersion; }


    /**
     * @brief      Gets the vulkan vendor id.
     *
     * @return     The vulkan vendor id.
     */
    uint32_t getVkVendorID() { return vkVendorID; }


    /**
     * @brief      Gets the vulkan device id.
     *
     * @return     The vulkan device id.
     */
    uint32_t getVkDeviceID() { return vkDeviceID; }


    /**
     * @brief      Gets the vulkan extensions.
     *
     * @return     The vulkan extensions.
     */
    std::vector<std::string> getVkExtensions() { return vkExtensions; }
};

}