/*__________________________________________________
 |                                                  |
 |  File: VkInfo.hpp                                |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Store the GPU and Vulkan info.     |
 |_________________________________________________*/



#pragma once


namespace gpu {


/**
 * Sample count.
 */
enum Count {
    VK_SAMPLE_COUNT_1_BIT = 0x00000001,
    VK_SAMPLE_COUNT_2_BIT = 0x00000002,
    VK_SAMPLE_COUNT_4_BIT = 0x00000004,
    VK_SAMPLE_COUNT_8_BIT = 0x00000008,
    VK_SAMPLE_COUNT_16_BIT = 0x00000010,
    VK_SAMPLE_COUNT_32_BIT = 0x00000020,
    VK_SAMPLE_COUNT_64_BIT = 0x00000040,
    VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF
};


/**
 * @brief      VkInfo struct used to store the GPU and OpenGL data.
 * Structure containing the Vulkan information.
 */
struct VkInfo {

    uint32_t              maxImageDimension1D = -1;
    uint32_t              maxImageDimension2D = -1;
    uint32_t              maxImageDimension3D = -1;
    uint32_t              maxImageDimensionCube = -1;
    uint32_t              maxImageArrayLayers = -1;
    uint32_t              maxTexelBufferElements = -1;
    uint32_t              maxUniformBufferRange = -1;
    uint32_t              maxStorageBufferRange = -1;
    uint32_t              maxPushConstantsSize = -1;
    uint32_t              maxMemoryAllocationCount = -1;
    uint32_t              maxSamplerAllocationCount = -1;
    uint64_t              bufferImageGranularity = -1;
    uint64_t              sparseAddressSpaceSize = -1;
    uint32_t              maxBoundDescriptorSets = -1;
    uint32_t              maxPerStageDescriptorSamplers = -1;
    uint32_t              maxPerStageDescriptorUniformBuffers = -1;
    uint32_t              maxPerStageDescriptorStorageBuffers = -1;
    uint32_t              maxPerStageDescriptorSampledImages = -1;
    uint32_t              maxPerStageDescriptorStorageImages = -1;
    uint32_t              maxPerStageDescriptorInputAttachments = -1;
    uint32_t              maxPerStageResources = -1;
    uint32_t              maxDescriptorSetSamplers = -1;
    uint32_t              maxDescriptorSetUniformBuffers = -1;
    uint32_t              maxDescriptorSetUniformBuffersDynamic = -1;
    uint32_t              maxDescriptorSetStorageBuffers = -1;
    uint32_t              maxDescriptorSetStorageBuffersDynamic = -1;
    uint32_t              maxDescriptorSetSampledImages = -1;
    uint32_t              maxDescriptorSetStorageImages = -1;
    uint32_t              maxDescriptorSetInputAttachments = -1;
    uint32_t              maxVertexInputAttributes = -1;
    uint32_t              maxVertexInputBindings = -1;
    uint32_t              maxVertexInputAttributeOffset = -1;
    uint32_t              maxVertexInputBindingStride = -1;
    uint32_t              maxVertexOutputComponents = -1;
    uint32_t              maxTessellationGenerationLevel = -1;
    uint32_t              maxTessellationPatchSize = -1;
    uint32_t              maxTessellationControlPerVertexInputComponents = -1;
    uint32_t              maxTessellationControlPerVertexOutputComponents = -1;
    uint32_t              maxTessellationControlPerPatchOutputComponents = -1;
    uint32_t              maxTessellationControlTotalOutputComponents = -1;
    uint32_t              maxTessellationEvaluationInputComponents = -1;
    uint32_t              maxTessellationEvaluationOutputComponents = -1;
    uint32_t              maxGeometryShaderInvocations = -1;
    uint32_t              maxGeometryInputComponents = -1;
    uint32_t              maxGeometryOutputComponents = -1;
    uint32_t              maxGeometryOutputVertices = -1;
    uint32_t              maxGeometryTotalOutputComponents = -1;
    uint32_t              maxFragmentInputComponents = -1;
    uint32_t              maxFragmentOutputAttachments = -1;
    uint32_t              maxFragmentDualSrcAttachments = -1;
    uint32_t              maxFragmentCombinedOutputResources = -1;
    uint32_t              maxComputeSharedMemorySize = -1;
    uint32_t              maxComputeWorkGroupCountX = -1;
    uint32_t              maxComputeWorkGroupCountY = -1;
    uint32_t              maxComputeWorkGroupCountZ = -1;
    uint32_t              maxComputeWorkGroupInvocations = -1;
    uint32_t              maxComputeWorkGroupSizeX = -1;
    uint32_t              maxComputeWorkGroupSizeY = -1;
    uint32_t              maxComputeWorkGroupSizeZ = -1;
    uint32_t              subPixelPrecisionBits = -1;
    uint32_t              subTexelPrecisionBits = -1;
    uint32_t              mipmapPrecisionBits = -1;
    uint32_t              maxDrawIndexedIndexValue = -1;
    uint32_t              maxDrawIndirectCount = -1;
    float                 maxSamplerLodBias = -1;
    float                 maxSamplerAnisotropy = -1;
    uint32_t              maxViewports = -1;
    uint32_t              maxViewportDimensionsX = -1;
    uint32_t              maxViewportDimensionsY = -1;
    float                 viewportBoundsRangeX = -1;
    float                 viewportBoundsRangeY = -1;
    uint32_t              viewportSubPixelBits = -1;
    size_t                minMemoryMapAlignment = -1;
    uint64_t              minTexelBufferOffsetAlignment = -1;
    uint64_t              minUniformBufferOffsetAlignment = -1;
    uint64_t              minStorageBufferOffsetAlignment = -1;
    int32_t               minTexelOffset = -1;
    uint32_t              maxTexelOffset = -1;
    int32_t               minTexelGatherOffset = -1;
    uint32_t              maxTexelGatherOffset = -1;
    float                 minInterpolationOffset = -1;
    float                 maxInterpolationOffset = -1;
    uint32_t              subPixelInterpolationOffsetBits = -1;
    uint32_t              maxFramebufferWidth = -1;
    uint32_t              maxFramebufferHeight = -1;
    uint32_t              maxFramebufferLayers = -1;
    Count                 framebufferColorSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 framebufferDepthSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 framebufferStencilSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 framebufferNoAttachmentsSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    uint32_t              maxColorAttachments = -1;
    Count                 sampledImageColorSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 sampledImageIntegerSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 sampledImageDepthSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 sampledImageStencilSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    Count                 storageImageSampleCounts = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    uint32_t              maxSampleMaskWords = -1;
    bool                  timestampComputeAndGraphics = -1;
    float                 timestampPeriod = -1;
    uint32_t              maxClipDistances = -1;
    uint32_t              maxCullDistances = -1;
    uint32_t              maxCombinedClipAndCullDistances = -1;
    uint32_t              discreteQueuePriorities = -1;
    float                 pointSizeRangeX = -1;
    float                 pointSizeRangeY = -1;
    float                 lineWidthRangeX = -1;
    float                 lineWidthRangeY = -1;
    float                 pointSizeGranularity = -1;
    float                 lineWidthGranularity = -1;
    bool                  strictLines = -1;
    bool                  standardSampleLocations = -1;
    uint64_t              optimalBufferCopyOffsetAlignment = -1;
    uint64_t              optimalBufferCopyRowPitchAlignment = -1;
    uint64_t              nonCoherentAtomSize = -1;
};
}