/*__________________________________________________
 |                                                  |
 |  File: Profiler.cpp                              |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Profiler used to get the GPU data  |
 |  during the tests.                               |
 |_________________________________________________*/



#include "Profiler.h"

using namespace gpu;



// Adds an entry to the profiler.
void Profiler::addTrianglesEntry(unsigned numberOfTriangles, unsigned time) {

    std::vector<unsigned> entry;
    entry.push_back(TYPE::TRIANGLES);
    entry.push_back(numberOfTriangles);
    entry.push_back(time);
    data.push_back(entry);
}


// Save to CSV.
bool Profiler::save(std::string filename) {

    // Open the file
    std::ofstream file(filename);

    if (!file.is_open()) return false;
    file << "Number of triangles, Time\n";

    // Go through the data and write it to file
    for (unsigned i = 0; i < data.size(); ++i)
        file << data[i][1] << ", " << data[i][2] << "\n";

    file.close();
    return true;
}