rm build -rf
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make -j 12
make install | egrep -v 'gmock|gtest'